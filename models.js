/**
 * Created by kimduk on 16.05.14.
 */
var mongoose = require('mongoose').Mongoose;

mongoose.model('Document', {
    properties: ['title', 'data', 'tags'],

    indexes: [
        'title'
    ]
});

exports.Document = function(db) {
    return db.model('Document');
};